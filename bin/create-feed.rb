#!/usr/bin/env ruby

require 'nokogiri'
require 'date'
require 'json'

# Set some variables
author = 'Mark Sue'
origin = 'https://www.youtube.com/playlist?list=PL4ix-28ynT4qOu4_H12qJA62JQIgp_62W'
name   = 'The GodBreaking - D&D 5e Campaign - Fantasy Grounds'

# Get all current episodes
Dir.chdir '/srv/TheGodbreaking/episodes'
episodes = Dir['*.info.json']

# Method to convert seconds to HH:MM:SS
def seconds_to_hms(sec)
  '%02d:%02d:%02d' % [sec / 3600, sec / 60 % 60, sec % 60]
end

builder = Nokogiri::XML::Builder.new { |xml|
  xml.rss 'xmlns:itunes': 'http://www.itunes.com/dtds/podcast-1.0.dtd',
          'xmlns:atom': 'http://www.w3.org/2005/Atom',
          version: '2.0' do
    xml.channel do
      xml.title name
      xml.link origin
      xml.image do
        xml.url 'https://i.ytimg.com/vi/M1vgG3rUK9c/maxresdefault.jpg'
        xml.title name
        xml.link origin
      end
      xml.description 'This a homebrewed campaign played on the Fantasy Grounds virtual tabletop platform.'
      xml.language 'en-us'
      xml.copyright author
      xml['atom'].link href: 'https://godbreaking.kervyn.de/podcast.xml',
                       rel: 'self',
                       type: 'application/rss+xml'
      xml['itunes'].author author
      xml['itunes'].summary 'This a homebrewed campaign played on the Fantasy Grounds virtual tabletop platform.'
      xml['itunes'].owner do
        xml['itunes'].name author
      end
      xml['itunes'].explicit 'No'
      xml['itunes'].keywords 'Dungeons and Dragons,TheGodbreaking,5th Edition,Fantasy Grounds,5e'
      xml['itunes'].image 'https://i.ytimg.com/vi/M1vgG3rUK9c/maxresdefault.jpg'
      xml['itunes'].category text: 'Games'
      xml.lastBuildDate DateTime.now.rfc2822
      episodes.each do |file|
        json     = File.read(file)
        episode  = JSON.parse(json)
        title    = episode['fulltitle'].sub(' - D&D 5e Homebrew - Fantasy Grounds', '').sub(',', ' -')
        size     = episode['filesize']
        duration = seconds_to_hms(episode['duration'])
        link     = 'https://godbreaking.kervyn.de/' + file.gsub('.info.json', '.m4a')
        image    = episode['thumbnail']
        date     = Date.parse(episode['upload_date']).strftime('%a, %e %b %Y %H:%M:%S %z')
        xml.item do
          xml.title title
          xml.link link
          xml.enclosure url: link,
                        length: size,
                        type: 'audio/m4a'
          xml.guid link
          xml['itunes'].duration duration
          xml['itunes'].image href: image
          xml['itunes'].explicit 'no'
          xml['itunes'].summary episode['description']
          xml.pubDate date
          xml.description episode['description']
        end
      end
    end
  end
}.to_xml
File.write('/srv/TheGodbreaking/episodes/feed.xml', builder)

